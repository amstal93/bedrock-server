#!/bin/bash

# VARIABLES
container=$(buildah from fedora-minimal)
release=$(curl -s https://www.minecraft.net/en-us/download/server/bedrock | awk -F / '/linux\/bedrock-server-/ { print $5 }' | cut -d - -f 3 | cut -d . -f 1,2,3,4)
archive=bedrock-server-${release}.zip
download=https://minecraft.azureedge.net/bin-linux/${archive}

# FUNCTIONS
build_bedrock(){
# BUILDAH
    printf "Setting image configuartion options\n"
    buildah config --author='@IMetZach' ${container}
    buildah config --label description='The Vanilla MineCraft Bedrock Server' ${container}
    buildah config --label Name='MineCraft Bedrock Server' ${container}
    buildah config --label version=${release} ${container}

    printf "Installing dependencies and updating packages\n"
    buildah run ${container} -- bash -c "microdnf install unzip && microdnf update && microdnf clean all && rm -rf /var/cache/dnf"

    printf "Creating the woking directory\n"
    buildah run ${container} -- mkdir -p /opt/minecraft/worlds
    buildah config --workingdir='/opt/minecraft' ${container}

    buildah add ${container} "${download}" '/opt/minecraft'
    buildah run ${container} -- unzip -n "${archive}"

    printf "Adding startup.sh\n"
    buildah add ${container} ./startup.sh '/opt/minecraft/'
    printf "Setting permissions\n"
    buildah run ${container} -- chmod +x /opt/minecraft/startup.sh

    printf "Exposing ports\n"
    buildah config --port 19132 ${container}
    buildah config --port 19133 ${container}

    buildah config --entrypoint "/opt/minecraft/startup.sh" ${container}
    buildah commit ${container} bedrock-server
}

# MAIN SCRIPT
if [[  $(expr length "${release}") -gt 4  ]]
then
    printf "Online version (${release}) checks out. Building image.\n"
    build_bedrock
    printf "Image build completed\n"
else
    printf "Online check returned invalid value:\t${release}\n"
fi
