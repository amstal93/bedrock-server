#!/bin/bash

# VARIBLES
onlineVer=$(curl -s https://www.minecraft.net/en-us/download/server/bedrock | awk -F / '/linux\/bedrock-server-/ { print $5 }' | cut -d - -f 3 | cut -d . -f 1,2,3,4)

# MAIN SCRIPT
if [  ${onlineVer} == ${LOCAL_VERSION}  ]
    then
    printf "No new version available.\n"
    else
    printf "New version ${onlineVer} available.\n"
fi

# curl --request PUT --header "PRIVATE-TOKEN: <your_access_token>" "https://www.gitlab.com/api/v4/projects/18203078/variables/LOCAL_VERSION" --form "value=updated value"